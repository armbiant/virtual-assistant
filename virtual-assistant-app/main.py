"""
Main module to execute all the services exposed in the UI
Author: William Arias
"""
import os
import sys
import json
import yaml
import requests
import logging
import streamlit as st
from streamlit_chat import message as st_message

st.set_page_config(page_title="Cloud Native Virtual Assistant", page_icon="🤖")
st.title("English Sparring Partner 🥊")

API_URL = "https://api-inference.huggingface.co/models/facebook/blenderbot-400M-distill"


def get_model_id(data):
    return data["hf_model"]["api_id"]


def process_yaml():
    with open("config.yaml") as f:
        return yaml.safe_load(f)


def get_text():
    input_text = st.text_input(label="You: ", value="Hi there!")
    return input_text


@st.cache
def query(payload, api_token):
    """ Pass query to Inference API and returns model generated answer"""
    data = json.dumps(payload)
    headers = {"Authorization": f"Bearer {api_token}"}
    response = requests.request("POST", API_URL, headers=headers, data=data)
    return json.loads(response.content.decode("utf-8"))


def main():
    """Executes all the functions"""

    # 1. Streamlit configuration

    if 'generated_responses' not in st.session_state:
        st.session_state['generated_responses'] = []

    if 'past_user_inputs' not in st.session_state:
        st.session_state['past_user_inputs'] = []

    # 2. Queries Model
    user_input_text = get_text()
    #config_yaml = process_yaml()
    #API_TOKEN = get_model_id(config_yaml)
    API_TOKEN = os.environ['MODEL_TOKEN']

    if user_input_text:
        logging.info("User input text")
        data = {"inputs": {
                            "past_user_inputs": st.session_state['past_user_inputs'],
                            "generated_responses": st.session_state["generated_responses"],
                            "text": user_input_text,
        }}

        model_response = query(payload=data, api_token=API_TOKEN)
        logging.info("Query to inference API")
        print(model_response)
        logging.info(model_response)

        st.session_state['past_user_inputs'].append(user_input_text)
        logging.info("Added to past_user-inputs tracker")
        st.session_state['generated_responses'].append(model_response["generated_text"])
        logging.info("Added to generated_responses tracker")

    # 3. Displays chat history with the model
    if st.session_state['generated_responses']:
        for index in range(len(st.session_state['generated_responses'])-1, -1, -1):
            st_message(st.session_state["generated_responses"][index], key=str(index))
            st_message(st.session_state['past_user_inputs'][index], is_user=True, key=str(index)+'_user',
                       avatar_style='pixel-art')


if __name__ == "__main__":
    main()
    handler_stdout = logging.StreamHandler(sys.stdout)
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s:%(message)s',
                        handlers=[logging.FileHandler('myapp.log'), handler_stdout])
