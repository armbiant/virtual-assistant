FROM python:3.8-slim-bullseye
LABEL maintainer="William Arias"
COPY ./virtual-assistant-app/  /app/virtual-assistant-app
WORKDIR /app/virtual-assistant-app
RUN pip install -r requirements.txt
EXPOSE 8501
ENTRYPOINT ["streamlit", "run"]
CMD ["main.py"]

