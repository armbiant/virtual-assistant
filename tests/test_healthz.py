import requests


def test_status():

    health = requests.get("http://localhost:8501/healthz")
    assert health.status_code == 200, "Should be 200"